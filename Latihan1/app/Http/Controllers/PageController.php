<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{   
    function home(){
        $angka1 = 2;
        $angka2 = 6;

        $total = $angka1 + $angka2;
        return view('welcome')->with(['total' => $total]);
    }
    function profile(){
        return view('profile');
    }
    function news(){
        return view('news');
    }
    function product(){
        return view('product');
    }
    function travel(){
        return view('travel');
    }
    function food(){
        return view('food');
    }
    function hitung(){
        $nilai = 85;
        
        return view('hitung')->with(['nilai' => $nilai]);
    }
}
?>
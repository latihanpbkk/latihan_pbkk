<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(){
        $data = Product::orderBy('id','desc')->get();
        return view('product.index', ['data' => $data]);
    }

    public function create(){
        return view('product.create');
    }

    public function save(Request $request){
        $product = New Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->save(); 
        return redirect('/product');
    }

    public function delete($id){
        $product = Product::where('id',$id)->delete();
        return redirect('/product');
    }

    public function edit($id){
        $product = Product::where('id',$id)->first();
        return view('product.edit',['product' => $product]);
    }

    public function update(Request $request){
        $product = Product::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'price' => $request->price,
                'description' => $request->description,
            ]
        );
        return redirect('/product');
    }
}

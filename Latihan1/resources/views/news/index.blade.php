@extends('layouts.app')
@section('title', 'Halaman News')
@section('main')
@guest
<div class="container">
    <div class="row mt-3 mb-3">
        @foreach($data as $news)
            <div class="col-3 mb-3">
            <a class="text-decoration-none text-black" href="{{ url('/news/detail/' .$news->id) }}">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ $news->title }}</h3>
                    </div>
                    <div class="card-body">
                        @if(strlen($news->description) > 100)
                            {{ substr_replace($news->description,"...", 100) }}
                        @else
                            {{ $news->description }}
                        @endif
                    </div>
                </div>
            </a>
            </div>
    @endforeach
    </div>
</div>

@else
<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-3" href="{{ url('/news/add') }}">Tambah Data</a>
        @foreach($data as $news)
            <div class="col-3 mb-3">
            <a class="text-decoration-none text-black" href="{{ url('/news/detail/' .$news->id) }}">
                <div class="card">
                    <div class="card-header">
                        <h3>{{ $news->title }}</h3>
                    </div>
                    <div class="card-body">
                        @if(strlen($news->description) > 100)
                            {{ substr_replace($news->description,"...", 100) }}
                        @else
                            {{ $news->description }}
                        @endif
                    </div>
                    <div class="card-footer">
                    <a href="{{ url('/news/edit/'.$news->id) }}" class="btn btn-warning btn-sm">Edit</a>
                    <a href="{{ url('/news/delete/'.$news->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                    </div>
                </div>
            </a>
            </div>
    @endforeach
    </div>
</div>
@endguest
@endsection
@extends('layouts.app')
@section('title', 'Halaman Product')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <form action="{{ url('/product/create') }}" method="post">
            @csrf
            <div class="mb-3">
                <label>Nama</label>
                <input type="text" class="form-control" name="name">
            </div> 
            <div class="mb-3">
                <label>Harga</label>
                <input type="text" class="form-control" name="price">
            <div class="mb-3">
                <label>Deskripsi</label>
                <textarea name="description" class="form-control"></textarea>
            </div> 
            <div class="mb-3">
                <button class="btn btn-primary">Submit</button>
            </div>  
        </form>
    </div>
</div>
@endsection
@extends('layouts.app')
@section('title', 'Halaman Product')
@section('main')
<div class="container">
    <div class="row mt-3 mb-3">
        <a class="btn btn-primary mb-3" href="{{ url('/product/add') }}">Tambah Data</a>
        @foreach($data as $product)
        <div class="col-3 mb-3">
            <div class="card">
                <div class="card-header">
                    <b>{{ $product->name }}</b> <i> Rp  {{ number_format 
                        ($product->price,2,",",".") }})</i>
                </div>
                <div class="card-body">
                    {{ $product->description }}
                </div>
                <div class="card-footer">
                  <a href="{{ url('/product/edit/'.$product->id) }}" class="btn btn-warning btn-sm">Edit</a>
                  <a href="{{ url('/product/delete/'.$product->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>

@endsection